<h1>Audio Visual Programming in GLFW C/C++</h1>

<h2>Writing 100 by 100 pixel Red color ( C++ GLFW )</h2>

<h3>Running the project</h3>

clone the project
```console
git clone https://gitlab.com/aqifcse/video-app.git
```
remove lib/glfw directory both from local and git cache
```console
cd video-app/lib
~/video-app/lib$ rm -rf -r glfw
~/video-app/lib$ git rm -r --cached glfw
```
Add the glfw submodule in the lib
```console
~/video-app/lib$ git submodule add https://github.com/glfw/glfw glfw
```
create a build directory in the app
```console
~/video-app/lib$ cd ..
~/video-app$ mkdir build
```
run cmake to create make and run the app

```console
~/video-app/build$ cmake ..
~/video-app/build$ make
~/video-app/build$ ./video-app
```
You will see this screen

![100_by_100_Pixels_Red](https://i.ibb.co/x63QLxv/Workspace-1-005.png)

But it is fixed in a screen. We can't put it anywhere in the screen. To do that we need Texture

OpenGL works by handling data as a texture.

After adding texture in the main.cpp code we get the following output -

![100_by_100_Pixels_Red&Blue_with_Textures](https://i.ibb.co/84ZjkvP/Screenshot-from-2020-02-22-22-13-00.png)
